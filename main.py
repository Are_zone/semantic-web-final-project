from rdflib import URIRef, BNode, Literal, Namespace, Graph, XSD
from rdflib.namespace import RDF
import csv

wd = Namespace('https://www.wikidata.org/wiki/')
wdt = Namespace('https://www.wikidata.org/wiki/Property:')
ex = Namespace('http://example.org/')

f = csv.reader(open('output.csv'))
a = list(f)[1:]

# graph
g = Graph()

def cek(nama_center):
    if nama_center == "Ames Research Center":
        return wd.Q181052
    elif nama_center == "Armstrong Flight Research Center":
        return wd.Q305443
    elif nama_center == "Glenn Research Center":
        return wd.Q618728
    elif nama_center == "Goddard Space Flight Center":
        return wd.Q52152
    elif nama_center == "Jet Propulsion Lab":
        return wd.Q189325
    elif nama_center == "Johnson Space Center":
        return wd.Q208371
    elif nama_center == "Kennedy Space Center":
        return wd.Q48821
    elif nama_center == "Langley Research Center":
        return wd.Q618731
    elif nama_center == "Marshall Space Flight Center":
        return wd.Q618696
    elif nama_center == "Michoud Assembly Facility":
        return wd.Q1441564
    elif nama_center == "NASA Aircraft Management Division":
        return wd.Q618673
    elif nama_center == "Stennis Space Center":
        return wd.Q618717     
    elif nama_center == "Wallops Flight Facility/GSFC":
        return wd.Q182348

def isifasilitas(indeks):
    lis = []
    for baris in a:
        node_parent = cek(baris[0])
        nilai_kolom = baris[indeks]
        node = BNode()
        g.add( ( node, ex.name, nilai_kolom ) )
        g.add( ( node, ex.partOf, node_parent ) )
        lis.append(node)
    return lis

def isi(indeks):
    lis = []
    for baris in a:
        kolom = baris[indeks]
        node = Literal(kolom)
        lis.append(node)
    return lis

def isitgl(indeks):
    lis = []
    for baris in a:
        kolom = baris[indeks]
        node = Literal(kolom, datatype=XSD.date)
        lis.append(node)
    return lis

# Facility
lis_facility = isi(2)
print(lis_facility)

# Occupied
lis_occupied = isitgl(3)
print(lis_occupied)

# Status
lis_status = isi(4)
print(lis_status)

# URL Link
lis_url = isi(5)
print(lis_url)

# Record Date
lis_record_date = isitgl(6)
print(lis_record_date)

# Last Update
lis_last_update = isitgl(7)
print(lis_last_update)


# ames research center CA   
g.add((wd.Q181052, RDF.type, wd.Q16986465)) # type nasa facilities
g.add((wd.Q181052, ex.location, Literal('37.41412, -122.052585'))) # location
g.add((wd.Q181052, ex.city , wd.Q110739)) # city
g.add((wd.Q181052, ex.state, wd.Q99)) # state
g.add((wd.Q181052, ex.country, wd.Q30)) # country
g.add((wd.Q181052, ex.zipcode, Literal('94035'))) # zipcode
g.add((wd.Q181052, ex.searchStatus, Literal('Public'))) # search status

# armstrong flight research center CA
g.add((wd.Q305443, RDF.type, wd.Q16986465))
g.add((wd.Q305443, ex.location, Literal('35.000989, -117.888837'))) # location
g.add((wd.Q305443, ex.city, wd.Q108047)) # city
g.add((wd.Q305443, ex.state, wd.Q99)) # state
g.add((wd.Q305443, ex.country, wd.Q30)) # country
g.add((wd.Q305443, ex.zipcode, Literal('93523'))) # zipcode
g.add((wd.Q305443, ex.searchStatus, Literal('Public'))) # search status

# glenn research center OH
g.add((wd.Q618728, RDF.type, wd.Q16986465))
g.add((wd.Q618728, ex.location, Literal('41.430364, -81.808561'))) # location
g.add((wd.Q618728, ex.city, wd.Q37320)) # city
g.add((wd.Q618728, ex.state, wd.Q1397)) # state
g.add((wd.Q618728, ex.country, wd.Q30)) # country
g.add((wd.Q618728, ex.zipcode, Literal('44135'))) # zipcode
g.add((wd.Q618728, ex.searchStatus, Literal('Public'))) # search status

# goddard space flight center MD   
g.add((wd.Q52152, RDF.type, wd.Q16986465))
g.add((wd.Q52152, ex.location, Literal('38.99538, -76.853161'))) # location
g.add((wd.Q52152, ex.city, wd.Q755877)) # city
g.add((wd.Q52152, ex.state, wd.Q1391)) # state
g.add((wd.Q52152, ex.country, wd.Q30)) # country
g.add((wd.Q52152, ex.zipcode, Literal('20771'))) # zipcode
g.add((wd.Q52152, ex.searchStatus, Literal('Public'))) # search status

# jet propulsion lab   CA      
g.add((wd.Q189325, RDF.type, wd.Q16986465))
g.add((wd.Q189325, ex.location, Literal('34.178124, -118.150662'))) # location
g.add((wd.Q189325, ex.city, wd.Q485176)) # city
g.add((wd.Q189325, ex.state, wd.Q99)) # state
g.add((wd.Q189325, ex.country, wd.Q30)) # country
g.add((wd.Q189325, ex.zipcode, Literal('91109'))) # zipcode
g.add((wd.Q189325, ex.searchStatus, Literal('Public'))) # search status

# johnson space center    TX 
g.add((wd.Q208371, RDF.type, wd.Q16986465))
g.add((wd.Q208371, ex.location, Literal('29.559177, -95.097897'))) # location
g.add((wd.Q208371, ex.city, wd.Q16555)) # city
g.add((wd.Q208371, ex.state, wd.Q1439)) # state
g.add((wd.Q208371, ex.country, wd.Q30)) # country
g.add((wd.Q208371, ex.zipcode, Literal('77058'))) # zipcode
g.add((wd.Q208371, ex.searchStatus, Literal('Public'))) # search status

# kennedy space center FL               
g.add((wd.Q48821, RDF.type, wd.Q16986465))
g.add((wd.Q48821, ex.location, Literal('28.538331, -81.378879'))) # location
g.add((wd.Q48821, ex.city, wd.Q488517)) # city
g.add((wd.Q48821, ex.state, wd.Q812)) # state
g.add((wd.Q48821, ex.country, wd.Q30)) # country
g.add((wd.Q48821, ex.zipcode, Literal('32899'))) # zipcode
g.add((wd.Q48821, ex.searchStatus, Literal('Public'))) # search status

# langley research center  VA           
g.add((wd.Q618731, RDF.type, wd.Q16986465))
g.add((wd.Q618731, ex.location, Literal('37.08681, -76.376649'))) # location
g.add((wd.Q618731, ex.city, wd.Q342043)) # city
g.add((wd.Q618731, ex.state, wd.Q1370)) # state
g.add((wd.Q618731, ex.country, wd.Q30)) # country
g.add((wd.Q618731, ex.zipcode, Literal('23681-2199'))) # zipcode
g.add((wd.Q618731, ex.searchStatus, Literal('Public'))) # search status

# marshall space flight center  AL      
g.add((wd.Q618696, RDF.type, wd.Q16986465))
g.add((wd.Q618696, ex.location, Literal('34.729538, -86.585283'))) # location
g.add((wd.Q618696, ex.city, wd.Q79860)) # city
g.add((wd.Q618696, ex.state, wd.Q173)) # state
g.add((wd.Q618696, ex.country, wd.Q30)) # country
g.add((wd.Q618696, ex.zipcode, Literal('35812'))) # zipcode
g.add((wd.Q618696, ex.searchStatus, Literal('Public'))) # search status

# michoud assembly facility  LA         
g.add((wd.Q1441564, RDF.type, wd.Q16986465))
g.add((wd.Q1441564, ex.location, Literal('29.950621, -90.074948'))) # location
g.add((wd.Q1441564, ex.city, wd.Q34404)) # city
g.add((wd.Q1441564, ex.state, wd.Q1588)) # state
g.add((wd.Q1441564, ex.country, wd.Q30)) # country
g.add((wd.Q1441564, ex.zipcode, Literal('70189'))) # zipcode
g.add((wd.Q1441564, ex.searchStatus, Literal('Public'))) # search status

# nasa aircraft management division  DC 
g.add((wd.Q618673, RDF.type, wd.Q16986465))
g.add((wd.Q618673, ex.location, Literal('38.883239, -77.016473'))) # location
g.add((wd.Q618673, ex.city, wd.Q61)) # city
g.add((wd.Q618673, ex.state, wd.Q1223)) # state
g.add((wd.Q618673, ex.country, wd.Q30)) # country
g.add((wd.Q618673, ex.zipcode, Literal('20546'))) # zipcode
g.add((wd.Q618673, ex.searchStatus, Literal('Public'))) # search status

# stennis space center  MS              
g.add((wd.Q618717, RDF.type, wd.Q16986465))
g.add((wd.Q618717, ex.location, Literal('30.385948, -89.604486'))) # location
g.add((wd.Q618717, ex.city, wd.Q485441)) # city
g.add((wd.Q618717, ex.state, wd.Q1494)) # state
g.add((wd.Q618717, ex.country, wd.Q30)) # country
g.add((wd.Q618717, ex.zipcode, Literal('39529-6000'))) # zipcode
g.add((wd.Q618717, ex.searchStatus, Literal('Public'))) # search status

# wallops flight facility / GSFC  VA    
g.add((wd.Q182348, RDF.type, wd.Q16986465))
g.add((wd.Q182348, ex.location, Literal('37.911289, -75.469622'))) # location
g.add((wd.Q182348, ex.city, wd.Q2110026)) # city
g.add((wd.Q182348, ex.state, wd.Q1370)) # state
g.add((wd.Q182348, ex.country, wd.Q30)) # country
g.add((wd.Q182348, ex.zipcode, Literal('23337-5099'))) # zipcode
g.add((wd.Q182348, ex.searchStatus, Literal('Public'))) # search status


for i in range(len(a)):
    node = ( lis_facility[i], ex.occupied_date,  lis_occupied[i])
    g.add(node)
    node = ( lis_facility[i], ex.status,  lis_status[i])
    g.add(node)
    node = ( lis_facility[i], ex.url,  lis_url[i])
    g.add(node)
    node = ( lis_facility[i], ex.record_date,  lis_record_date[i])
    g.add(node)
    node = ( lis_facility[i], ex.last_update_date,  lis_last_update[i])
    g.add(node)

q = g.serialize(destination='rdf.ttl', format='turtle')